﻿using DatabaseAccess.DbAccess;
using DatabaseAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseAccess.Data
{
    public class UserData
    {
        private readonly ISqlDataAccess _db;

        public UserData(ISqlDataAccess db)
        {
            _db = db;
        }

        public Task<IEnumerable<User>> GetUsers() =>
            _db.LoadData<User, dynamic>("spUser_GetAll", new { });

        public async Task<User> GetUser(int id)
        {
            var results = await _db.LoadData<User, dynamic>("spUser_Get", new { Id = id });
            return results.FirstOrDefault();
        }

        public Task InsertData(User user) =>
            _db.SaveData("spUser_Insert", new { user.FirstName, user.LastName });

        public Task UpdateData(User user) =>
            _db.SaveData("spUser_Update", user);

        public Task DeleteData(int id) =>
            _db.SaveData("spUser_Delete", new {Id = id});
    }
}
